package com.paiwad.myapp.pwhotelsinfo.data

import com.paiwad.myapp.pwhotelsinfo.data.model.locationsSearch.HotelsSearchResponse
import com.paiwad.myapp.pwhotelsinfo.data.model.photos.PhotosResponse
import com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail.PopertiesDetailResponse
import com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList.PopertiesResponse
import com.paiwad.myapp.pwhotelsinfo.data.model.reviews.ReviewsResponse
import com.paiwad.myapp.pwhotelsinfo.data.remote.HotelsRapidApi
import com.paiwad.myapp.pwhotelsinfo.data.repository.HotelRepository
import io.reactivex.rxjava3.core.Observable

class HotelRepositoryImpl(
    private val remoteDataSource: HotelsRapidApi
): HotelRepository {
    override fun searchWithLocations(query: String): Observable<HotelsSearchResponse> {
       return remoteDataSource.locationsSearch(query)
    }

    override fun getProperties(
        destinationId: String,
        checkOut: String,
        checkIn: String
    ): Observable<PopertiesResponse> {
        return remoteDataSource.getProperties(
            destinationId = destinationId,
            checkIn = checkIn,
            checkOut = checkOut
        )
    }

    override fun getPropertiesDetail(
        id: String,
        checkOut: String,
        checkIn: String
    ): Observable<PopertiesDetailResponse> {
        return remoteDataSource.getPropertiesDetail(
            id = id,
            checkOut = checkOut,
            checkIn = checkIn
        )
    }

    override fun getHotelReviews(id: String): Observable<ReviewsResponse> {
        return remoteDataSource.getHotelReviews(id)
    }

    override fun getHotelPhotos(id: String): Observable<PhotosResponse> {
        return remoteDataSource.getHotelPhotos(id)
    }
}