package com.paiwad.myapp.pwhotelsinfo.data.model.locationsSearch


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Entity(
    @SerializedName("caption")
    val caption: String,
    @SerializedName("destinationId")
    val destinationId: String,
    @SerializedName("geoId")
    val geoId: String,
    @SerializedName("landmarkCityDestinationId")
    val landmarkCityDestinationId: Any,
    @SerializedName("latitude")
    val latitude: Double,
    @SerializedName("longitude")
    val longitude: Double,
    @SerializedName("name")
    val name: String,
    @SerializedName("redirectPage")
    val redirectPage: String,
    @SerializedName("searchDetail")
    val searchDetail: Any,
    @SerializedName("type")
    val type: String
): Serializable