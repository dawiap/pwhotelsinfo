package com.paiwad.myapp.pwhotelsinfo.data.model.locationsSearch


import com.google.gson.annotations.SerializedName

data class HotelsSearchResponse(
    @SerializedName("autoSuggestInstance")
    val autoSuggestInstance: Any,
    @SerializedName("geocodeFallback")
    val geocodeFallback: Boolean,
    @SerializedName("misspellingfallback")
    val misspellingFallback: Boolean,
    @SerializedName("moresuggestions")
    val moresSuggestions: Int,
    @SerializedName("suggestions")
    val suggestions: List<Suggestion>,
    @SerializedName("term")
    val term: String,
    @SerializedName("trackingID")
    val trackingID: String
)