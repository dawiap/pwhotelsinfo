package com.paiwad.myapp.pwhotelsinfo.data.model.locationsSearch


import com.google.gson.annotations.SerializedName

data class Suggestion(
    @SerializedName("entities")
    val entities: List<Entity>,
    @SerializedName("group")
    val group: String
)