package com.paiwad.myapp.pwhotelsinfo.data.model.photos


import com.google.gson.annotations.SerializedName

data class HotelImage(
    @SerializedName("baseUrl")
    val baseUrl: String,
    @SerializedName("imageId")
    val imageId: Int,
    @SerializedName("mediaGUID")
    val mediaGUID: String,
    @SerializedName("sizes")
    val sizes: List<Size>,
    @SerializedName("trackingDetails")
    val trackingDetails: TrackingDetails
)