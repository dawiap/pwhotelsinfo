package com.paiwad.myapp.pwhotelsinfo.data.model.photos


import com.google.gson.annotations.SerializedName

data class Image(
    @SerializedName("baseUrl")
    val baseUrl: String,
    @SerializedName("imageId")
    val imageId: Int,
    @SerializedName("mediaGUID")
    val mediaGUID: String,
    @SerializedName("sizes")
    val sizes: List<SizeX>
)