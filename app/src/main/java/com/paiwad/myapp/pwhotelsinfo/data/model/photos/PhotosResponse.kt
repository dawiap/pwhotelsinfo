package com.paiwad.myapp.pwhotelsinfo.data.model.photos


import com.google.gson.annotations.SerializedName

data class PhotosResponse(
    @SerializedName("featuredImageTrackingDetails")
    val featuredImageTrackingDetails: FeaturedImageTrackingDetails,
    @SerializedName("hotelId")
    val hotelId: Int,
    @SerializedName("hotelImages")
    val hotelImages: List<HotelImage>,
    @SerializedName("propertyImageTrackingDetails")
    val propertyImageTrackingDetails: PropertyImageTrackingDetails,
    @SerializedName("roomImages")
    val roomImages: List<RoomImage>
)