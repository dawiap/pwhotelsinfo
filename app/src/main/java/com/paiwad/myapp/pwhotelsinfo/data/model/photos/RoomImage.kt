package com.paiwad.myapp.pwhotelsinfo.data.model.photos


import com.google.gson.annotations.SerializedName

data class RoomImage(
    @SerializedName("images")
    val images: List<Image>,
    @SerializedName("roomId")
    val roomId: Int
)