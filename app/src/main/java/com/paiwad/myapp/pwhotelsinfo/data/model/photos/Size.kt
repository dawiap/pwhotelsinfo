package com.paiwad.myapp.pwhotelsinfo.data.model.photos


import com.google.gson.annotations.SerializedName

data class Size(
    @SerializedName("suffix")
    val suffix: String,
    @SerializedName("type")
    val type: Int
)