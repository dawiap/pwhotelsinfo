package com.paiwad.myapp.pwhotelsinfo.data.model.photos


import com.google.gson.annotations.SerializedName

data class SizeX(
    @SerializedName("suffix")
    val suffix: String,
    @SerializedName("type")
    val type: Int
)