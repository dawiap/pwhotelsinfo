package com.paiwad.myapp.pwhotelsinfo.data.model.photos


import com.google.gson.annotations.SerializedName

data class TrackingDetails(
    @SerializedName("algorithmName")
    val algorithmName: String,
    @SerializedName("namespace")
    val namespace: String,
    @SerializedName("version")
    val version: String
)