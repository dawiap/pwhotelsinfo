package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class Amenity(
    @SerializedName("heading")
    val heading: String,
    @SerializedName("listItems")
    val listItems: List<Items>
)