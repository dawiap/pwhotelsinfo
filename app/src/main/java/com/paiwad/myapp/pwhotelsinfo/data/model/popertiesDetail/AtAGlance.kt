package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class AtAGlance(
    @SerializedName("keyFacts")
    val keyFacts: KeyFacts,
    @SerializedName("transportAndOther")
    val transportAndOther: TransportAndOther,
    @SerializedName("travellingOrInternet")
    val travellingOrInternet: TravellingOrInternet
)