package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class Body(
    @SerializedName("amenities")
    val amenities: List<Amenity>,
    @SerializedName("atAGlance")
    val atAGlance: AtAGlance,
    @SerializedName("guestReviews")
    val guestReviews: GuestReviews,
    @SerializedName("hotelWelcomeRewards")
    val hotelWelcomeRewards: HotelWelcomeRewards,
    @SerializedName("hygieneAndCleanliness")
    val hygieneAndCleanliness: HygieneAndCleanliness,
    @SerializedName("miscellaneous")
    val miscellaneous: Miscellaneous,
    @SerializedName("overview")
    val overview: Overview,
    @SerializedName("pageInfo")
    val pageInfo: PageInfo,
    @SerializedName("pdpHeader")
    val pdpHeader: PdpHeader,
    @SerializedName("propertyDescription")
    val propertyDescription: PropertyDescription,
    @SerializedName("smallPrint")
    val smallPrint: SmallPrint,
    @SerializedName("specialFeatures")
    val specialFeatures: SpecialFeatures,
    @SerializedName("unavailable")
    val unavailable: Unavailable
)