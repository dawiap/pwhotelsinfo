package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class Brands(
    @SerializedName("badgeText")
    val badgeText: String,
    @SerializedName("formattedRating")
    val formattedRating: String,
    @SerializedName("formattedScale")
    val formattedScale: String,
    @SerializedName("lowRating")
    val lowRating: Boolean,
    @SerializedName("rating")
    val rating: Double,
    @SerializedName("scale")
    val scale: Int,
    @SerializedName("total")
    val total: Int
)