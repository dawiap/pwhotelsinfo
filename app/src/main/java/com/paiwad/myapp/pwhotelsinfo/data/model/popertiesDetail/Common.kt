package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class Common(
    @SerializedName("pointOfSale")
    val pointOfSale: PointOfSale,
    @SerializedName("tracking")
    val tracking: Tracking
)