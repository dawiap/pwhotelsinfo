package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class Coordinates(
    @SerializedName("latitude")
    val latitude: Double,
    @SerializedName("longitude")
    val longitude: Double
)