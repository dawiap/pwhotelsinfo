package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class CurrentPrice(
    @SerializedName("formatted")
    val formatted: String,
    @SerializedName("plain")
    val plain: Int
)