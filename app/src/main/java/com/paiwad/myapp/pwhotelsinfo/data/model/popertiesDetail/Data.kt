package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("body")
    val body: Body,
    @SerializedName("common")
    val common: Common
)