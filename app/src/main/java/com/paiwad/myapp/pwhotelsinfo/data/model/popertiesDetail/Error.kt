package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class Error(
    @SerializedName("errorMessages")
    val errorMessages: List<String>,
    @SerializedName("fieldName")
    val fieldName: String
)