package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class FeaturedPrice(
    @SerializedName("afterPriceText")
    val afterPriceText: String,
    @SerializedName("beforePriceText")
    val beforePriceText: String,
    @SerializedName("bookNowButton")
    val bookNowButton: Boolean,
    @SerializedName("currentPrice")
    val currentPrice: CurrentPrice,
    @SerializedName("pricingAvailability")
    val pricingAvailability: String,
    @SerializedName("pricingTooltip")
    val pricingTooltip: String,
    @SerializedName("taxInclusiveFormatting")
    val taxInclusiveFormatting: Boolean
)