package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class GuestReviews(
    @SerializedName("brands")
    val brands: Brands
)