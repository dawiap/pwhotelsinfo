package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class HealthAndSafetyMeasures(
    @SerializedName("description")
    val description: String,
    @SerializedName("measures")
    val measures: List<String>,
    @SerializedName("title")
    val title: String
)