package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class HotelLocation(
    @SerializedName("coordinates")
    val coordinates: Coordinates,
    @SerializedName("locationName")
    val locationName: String,
    @SerializedName("resolvedLocation")
    val resolvedLocation: String
)