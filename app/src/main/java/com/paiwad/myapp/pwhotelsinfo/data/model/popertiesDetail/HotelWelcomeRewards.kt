package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class HotelWelcomeRewards(
    @SerializedName("applies")
    val applies: Boolean,
    @SerializedName("info")
    val info: String
)