package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class HygieneAndCleanliness(
    @SerializedName("healthAndSafetyMeasures")
    val healthAndSafetyMeasures: HealthAndSafetyMeasures,
    @SerializedName("title")
    val title: String
)