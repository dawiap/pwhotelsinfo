package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class Items(
    @SerializedName("heading")
    val heading: String,
    @SerializedName("listItems")
    val listItems: List<String>
)