package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class KeyFacts(
    @SerializedName("arrivingLeaving")
    val arrivingLeaving: List<String>,
    @SerializedName("hotelSize")
    val hotelSize: List<String>,
    @SerializedName("requiredAtCheckIn")
    val requiredAtCheckIn: List<String>,
    @SerializedName("specialCheckInInstructions")
    val specialCheckInInstructions: List<String>
)