package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class LocalisedAddress(
    @SerializedName("addressLine1")
    val addressLine1: String,
    @SerializedName("addressLine2")
    val addressLine2: String,
    @SerializedName("cityName")
    val cityName: String,
    @SerializedName("countryCode")
    val countryCode: String,
    @SerializedName("countryName")
    val countryName: String,
    @SerializedName("fullAddress")
    val fullAddress: String,
    @SerializedName("pattern")
    val pattern: String,
    @SerializedName("postalCode")
    val postalCode: String
)