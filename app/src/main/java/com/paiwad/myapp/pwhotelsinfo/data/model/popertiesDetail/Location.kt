package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class Location(
    @SerializedName("distance")
    val distance: String,
    @SerializedName("distanceInTime")
    val distanceInTime: String,
    @SerializedName("name")
    val name: String
)