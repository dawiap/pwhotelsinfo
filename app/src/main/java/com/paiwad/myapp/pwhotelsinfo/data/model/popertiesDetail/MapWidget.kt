package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class MapWidget(
    @SerializedName("staticMapUrl")
    val staticMapUrl: String
)