package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class Miscellaneous(
    @SerializedName("legalInfoForStrikethroughPrices")
    val legalInfoForStrikethroughPrices: String,
    @SerializedName("pimmsAttributes")
    val pimmsAttributes: String,
    @SerializedName("showLegalInfoForStrikethroughPrices")
    val showLegalInfoForStrikethroughPrices: Boolean
)