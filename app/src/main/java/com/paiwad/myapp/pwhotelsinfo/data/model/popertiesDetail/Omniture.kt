package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class Omniture(
    @SerializedName("s.currencyCode")
    val sCurrencyCode: String,
    @SerializedName("s.eVar13")
    val sEVar13: String,
    @SerializedName("s.eVar16")
    val sEVar16: String,
    @SerializedName("s.eVar26")
    val sEVar26: String,
    @SerializedName("s.eVar29")
    val sEVar29: String,
    @SerializedName("s.eVar31")
    val sEVar31: String,
    @SerializedName("s.eVar32")
    val sEVar32: String,
    @SerializedName("s.eVar34")
    val sEVar34: String,
    @SerializedName("s.eVar4")
    val sEVar4: String,
    @SerializedName("s.eVar40")
    val sEVar40: String,
    @SerializedName("s.eVar41")
    val sEVar41: String,
    @SerializedName("s.eVar43")
    val sEVar43: String,
    @SerializedName("s.eVar69")
    val sEVar69: String,
    @SerializedName("s.eVar80")
    val sEVar80: String,
    @SerializedName("s.eVar93")
    val sEVar93: String,
    @SerializedName("s.eVar95")
    val sEVar95: String,
    @SerializedName("s.products")
    val sProducts: String,
    @SerializedName("s.prop27")
    val sProp27: String,
    @SerializedName("s.prop28")
    val sProp28: String,
    @SerializedName("s.prop34")
    val sProp34: String,
    @SerializedName("s.prop36")
    val sProp36: String,
    @SerializedName("s.prop48")
    val sProp48: String,
    @SerializedName("s.prop5")
    val sProp5: String,
    @SerializedName("s.server")
    val sServer: String
)