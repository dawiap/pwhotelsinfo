package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class Overview(
    @SerializedName("overviewSections")
    val overviewSections: List<OverviewSection>
)