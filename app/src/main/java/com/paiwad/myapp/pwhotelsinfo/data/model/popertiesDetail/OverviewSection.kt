package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class OverviewSection(
    @SerializedName("content")
    val content: List<String>,
    @SerializedName("contentType")
    val contentType: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("type")
    val type: String
)