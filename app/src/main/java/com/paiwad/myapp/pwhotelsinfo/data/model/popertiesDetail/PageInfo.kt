package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class PageInfo(
    @SerializedName("errorKeys")
    val errorKeys: List<String>,
    @SerializedName("errors")
    val errors: List<Error>,
    @SerializedName("pageType")
    val pageType: String
)