package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class PdpHeader(
    @SerializedName("currencyCode")
    val currencyCode: String,
    @SerializedName("destinationId")
    val destinationId: String,
    @SerializedName("hotelId")
    val hotelId: String,
    @SerializedName("hotelLocation")
    val hotelLocation: HotelLocation,
    @SerializedName("occupancyKey")
    val occupancyKey: String,
    @SerializedName("pointOfSaleId")
    val pointOfSaleId: String
)