package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class PointOfSale(
    @SerializedName("brandName")
    val brandName: String,
    @SerializedName("numberSeparators")
    val numberSeparators: String
)