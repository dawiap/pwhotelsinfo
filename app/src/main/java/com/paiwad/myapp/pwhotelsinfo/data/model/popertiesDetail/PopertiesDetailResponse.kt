package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class PopertiesDetailResponse(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("neighborhood")
    val neighborhood: Neighborhood,
    @SerializedName("result")
    val result: String,
    @SerializedName("transportation")
    val transportation: Transportation
)