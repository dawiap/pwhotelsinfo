package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class PropertyDescription(
    @SerializedName("address")
    val address: Address,
    @SerializedName("clientToken")
    val clientToken: String,
    @SerializedName("featuredPrice")
    val featuredPrice: FeaturedPrice,
    @SerializedName("freebies")
    val freebies: List<String>,
    @SerializedName("localisedAddress")
    val localisedAddress: LocalisedAddress,
    @SerializedName("mapWidget")
    val mapWidget: MapWidget,
    @SerializedName("name")
    val name: String,
    @SerializedName("priceMatchEnabled")
    val priceMatchEnabled: Boolean,
    @SerializedName("roomTypeNames")
    val roomTypeNames: List<String>,
    @SerializedName("starRating")
    val starRating: Float,
    @SerializedName("starRatingTitle")
    val starRatingTitle: String,
    @SerializedName("tagline")
    val tagline: List<String>
)