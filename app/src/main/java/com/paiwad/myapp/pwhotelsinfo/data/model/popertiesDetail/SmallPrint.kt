package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class SmallPrint(
    @SerializedName("alternativeNames")
    val alternativeNames: List<String>,
    @SerializedName("display")
    val display: Boolean,
    @SerializedName("mandatoryFees")
    val mandatoryFees: List<Any>,
    @SerializedName("mandatoryTaxesOrFees")
    val mandatoryTaxesOrFees: Boolean,
    @SerializedName("optionalExtras")
    val optionalExtras: List<String>,
    @SerializedName("policies")
    val policies: List<String>
)