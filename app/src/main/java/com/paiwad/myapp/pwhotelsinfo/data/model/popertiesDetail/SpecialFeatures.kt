package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class SpecialFeatures(
    @SerializedName("sections")
    val sections: List<Any>
)