package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class Tracking(
    @SerializedName("omniture")
    val omniture: Omniture,
    @SerializedName("pageViewBeaconUrl")
    val pageViewBeaconUrl: String
)