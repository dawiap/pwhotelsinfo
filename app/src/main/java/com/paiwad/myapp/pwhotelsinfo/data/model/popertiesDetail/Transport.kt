package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class Transport(
    @SerializedName("offsiteTransfer")
    val offsiteTransfer: List<Any>,
    @SerializedName("parking")
    val parking: List<String>,
    @SerializedName("transfers")
    val transfers: List<String>
)