package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class TransportAndOther(
    @SerializedName("otherInclusions")
    val otherInclusions: List<Any>,
    @SerializedName("otherInformation")
    val otherInformation: List<Any>,
    @SerializedName("transport")
    val transport: Transport
)