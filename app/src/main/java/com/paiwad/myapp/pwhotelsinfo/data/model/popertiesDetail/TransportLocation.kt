package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class TransportLocation(
    @SerializedName("category")
    val category: String,
    @SerializedName("locations")
    val locations: List<Location>
)