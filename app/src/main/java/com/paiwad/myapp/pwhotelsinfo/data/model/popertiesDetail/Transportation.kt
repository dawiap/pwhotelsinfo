package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class Transportation(
    @SerializedName("transportLocations")
    val transportLocations: List<TransportLocation>
)