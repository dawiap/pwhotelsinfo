package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class Travelling(
    @SerializedName("children")
    val children: List<String>,
    @SerializedName("extraPeople")
    val extraPeople: List<Any>,
    @SerializedName("pets")
    val pets: List<String>
)