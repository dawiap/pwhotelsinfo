package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail


import com.google.gson.annotations.SerializedName

data class TravellingOrInternet(
    @SerializedName("internet")
    val internet: List<String>,
    @SerializedName("travelling")
    val travelling: Travelling
)