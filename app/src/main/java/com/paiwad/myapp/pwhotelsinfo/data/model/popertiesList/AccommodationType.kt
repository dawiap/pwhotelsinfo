package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class AccommodationType(
    @SerializedName("applied")
    val applied: Boolean,
    @SerializedName("items")
    val items: List<ItemX>
)