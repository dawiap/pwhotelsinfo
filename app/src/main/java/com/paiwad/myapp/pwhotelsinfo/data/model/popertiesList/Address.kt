package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class Address(
    @SerializedName("countryCode")
    val countryCode: String,
    @SerializedName("countryName")
    val countryName: String,
    @SerializedName("extendedAddress")
    val extendedAddress: String,
    @SerializedName("locality")
    val locality: String,
    @SerializedName("obfuscate")
    val obfuscate: Boolean,
    @SerializedName("postalCode")
    val postalCode: String,
    @SerializedName("region")
    val region: String,
    @SerializedName("streetAddress")
    val streetAddress: String
)