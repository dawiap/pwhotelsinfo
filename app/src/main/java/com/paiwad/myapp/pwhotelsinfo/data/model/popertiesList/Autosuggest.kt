package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class Autosuggest(
    @SerializedName("additionalUrlParams")
    val additionalUrlParams: AdditionalUrlParams
)