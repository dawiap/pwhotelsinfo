package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class Body(
    @SerializedName("filters")
    val filters: Filters,
    @SerializedName("header")
    val header: String,
    @SerializedName("miscellaneous")
    val miscellaneous: Miscellaneous,
    @SerializedName("pageInfo")
    val pageInfo: PageInfo,
    @SerializedName("pointOfSale")
    val pointOfSale: PointOfSale,
    @SerializedName("query")
    val query: Query,
    @SerializedName("searchResults")
    val searchResults: SearchResults,
    @SerializedName("sortResults")
    val sortResults: SortResults
)