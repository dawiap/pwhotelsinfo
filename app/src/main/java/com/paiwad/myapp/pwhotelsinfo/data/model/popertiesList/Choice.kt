package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class Choice(
    @SerializedName("label")
    val label: String,
    @SerializedName("selected")
    val selected: Boolean,
    @SerializedName("value")
    val value: String
)