package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class ChoiceX(
    @SerializedName("id")
    val id: Int,
    @SerializedName("label")
    val label: String
)