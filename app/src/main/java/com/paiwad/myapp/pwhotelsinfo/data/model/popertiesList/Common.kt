package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class Common(
    @SerializedName("pointOfSale")
    val pointOfSale: PointOfSaleX,
    @SerializedName("tracking")
    val tracking: Tracking
)