package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class Currency(
    @SerializedName("code")
    val code: String,
    @SerializedName("format")
    val format: String,
    @SerializedName("separators")
    val separators: String,
    @SerializedName("symbol")
    val symbol: String
)