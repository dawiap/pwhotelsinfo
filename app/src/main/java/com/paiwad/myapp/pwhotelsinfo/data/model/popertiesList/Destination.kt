package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class Destination(
    @SerializedName("id")
    val id: String,
    @SerializedName("resolvedLocation")
    val resolvedLocation: String,
    @SerializedName("value")
    val value: String
)