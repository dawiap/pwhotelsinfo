package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class EnhancedChoice(
    @SerializedName("choices")
    val choices: List<ChoiceX>,
    @SerializedName("itemMeta")
    val itemMeta: String,
    @SerializedName("label")
    val label: String
)