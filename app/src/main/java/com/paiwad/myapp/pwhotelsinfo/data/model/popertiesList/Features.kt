package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class Features(
    @SerializedName("noCCRequired")
    val noCCRequired: Boolean,
    @SerializedName("paymentPreference")
    val paymentPreference: Boolean
)