package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class Filters(
    @SerializedName("accessibility")
    val accessibility: Accessibility,
    @SerializedName("accommodationType")
    val accommodationType: AccommodationType,
    @SerializedName("applied")
    val applied: Boolean,
    @SerializedName("facilities")
    val facilities: Facilities,
    @SerializedName("guestRating")
    val guestRating: GuestRating,
    @SerializedName("landmarks")
    val landmarks: Landmarks,
    @SerializedName("name")
    val name: Name,
    @SerializedName("price")
    val price: Price,
    @SerializedName("starRating")
    val starRating: StarRating,
    @SerializedName("themesAndTypes")
    val themesAndTypes: ThemesAndTypes,
    @SerializedName("welcomeRewards")
    val welcomeRewards: WelcomeRewards
)