package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class GuestRating(
    @SerializedName("range")
    val range: Range
)