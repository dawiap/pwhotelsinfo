package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class GuestReviews(
    @SerializedName("badge")
    val badge: String,
    @SerializedName("badgeText")
    val badgeText: String,
    @SerializedName("rating")
    val rating: String,
    @SerializedName("scale")
    val scale: Int,
    @SerializedName("total")
    val total: Int,
    @SerializedName("unformattedRating")
    val unformattedRating: Float
)