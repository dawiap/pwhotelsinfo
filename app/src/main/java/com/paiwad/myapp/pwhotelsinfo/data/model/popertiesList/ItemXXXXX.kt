package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class ItemXXXXX(
    @SerializedName("disabled")
    val disabled: Boolean,
    @SerializedName("value")
    val value: Int
)