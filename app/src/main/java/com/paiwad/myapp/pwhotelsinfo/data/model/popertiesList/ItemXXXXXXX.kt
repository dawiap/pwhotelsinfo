package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class ItemXXXXXXX(
    @SerializedName("label")
    val label: String,
    @SerializedName("value")
    val value: String
)