package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class Landmark(
    @SerializedName("distance")
    val distance: String,
    @SerializedName("label")
    val label: String
)