package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class Landmarks(
    @SerializedName("distance")
    val distance: List<Any>,
    @SerializedName("items")
    val items: List<ItemXXX>,
    @SerializedName("selectedOrder")
    val selectedOrder: List<Any>
)