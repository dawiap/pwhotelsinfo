package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class Max(
    @SerializedName("defaultValue")
    val defaultValue: Int
)