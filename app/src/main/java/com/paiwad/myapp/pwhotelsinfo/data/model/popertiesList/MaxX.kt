package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class MaxX(
    @SerializedName("defaultValue")
    val defaultValue: Int
)