package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class MinX(
    @SerializedName("defaultValue")
    val defaultValue: Int
)