package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class Miscellaneous(
    @SerializedName("legalInfoForStrikethroughPrices")
    val legalInfoForStrikethroughPrices: String,
    @SerializedName("pageViewBeaconUrl")
    val pageViewBeaconUrl: String,
    @SerializedName("showLegalInfoForStrikethroughPrices")
    val showLegalInfoForStrikethroughPrices: Boolean
)