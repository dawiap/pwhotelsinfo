package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class Name(
    @SerializedName("autosuggest")
    val autosuggest: Autosuggest,
    @SerializedName("item")
    val item: ItemXXXX
)