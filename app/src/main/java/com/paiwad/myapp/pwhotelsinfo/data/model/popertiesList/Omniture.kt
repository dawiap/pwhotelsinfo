package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class Omniture(
    @SerializedName("s.currencyCode")
    val sCurrencyCode: String,
    @SerializedName("s.eVar13")
    val sEVar13: String,
    @SerializedName("s.eVar16")
    val sEVar16: String,
    @SerializedName("s.eVar2")
    val sEVar2: String,
    @SerializedName("s.eVar24")
    val sEVar24: String,
    @SerializedName("s.eVar26")
    val sEVar26: String,
    @SerializedName("s.eVar29")
    val sEVar29: String,
    @SerializedName("s.eVar31")
    val sEVar31: String,
    @SerializedName("s.eVar32")
    val sEVar32: String,
    @SerializedName("s.eVar33")
    val sEVar33: String,
    @SerializedName("s.eVar34")
    val sEVar34: String,
    @SerializedName("s.eVar4")
    val sEVar4: String,
    @SerializedName("s.eVar40")
    val sEVar40: String,
    @SerializedName("s.eVar41")
    val sEVar41: String,
    @SerializedName("s.eVar42")
    val sEVar42: String,
    @SerializedName("s.eVar43")
    val sEVar43: String,
    @SerializedName("s.eVar6")
    val sEVar6: String,
    @SerializedName("s.eVar63")
    val sEVar63: String,
    @SerializedName("s.eVar69")
    val sEVar69: String,
    @SerializedName("s.eVar7")
    val sEVar7: String,
    @SerializedName("s.eVar9")
    val sEVar9: String,
    @SerializedName("s.eVar93")
    val sEVar93: String,
    @SerializedName("s.eVar95")
    val sEVar95: String,
    @SerializedName("s.events")
    val sEvents: String,
    @SerializedName("s.products")
    val sProducts: String,
    @SerializedName("s.prop14")
    val sProp14: String,
    @SerializedName("s.prop15")
    val sProp15: String,
    @SerializedName("s.prop18")
    val sProp18: String,
    @SerializedName("s.prop2")
    val sProp2: String,
    @SerializedName("s.prop27")
    val sProp27: String,
    @SerializedName("s.prop29")
    val sProp29: String,
    @SerializedName("s.prop3")
    val sProp3: String,
    @SerializedName("s.prop32")
    val sProp32: String,
    @SerializedName("s.prop33")
    val sProp33: String,
    @SerializedName("s.prop36")
    val sProp36: String,
    @SerializedName("s.prop5")
    val sProp5: String,
    @SerializedName("s.prop7")
    val sProp7: String,
    @SerializedName("s.prop74")
    val sProp74: String,
    @SerializedName("s.prop9")
    val sProp9: String,
    @SerializedName("s.server")
    val sServer: String
)