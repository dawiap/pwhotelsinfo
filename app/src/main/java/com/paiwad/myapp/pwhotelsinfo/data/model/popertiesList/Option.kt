package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class Option(
    @SerializedName("choices")
    val choices: List<Choice>,
    @SerializedName("enhancedChoices")
    val enhancedChoices: List<EnhancedChoice>,
    @SerializedName("itemMeta")
    val itemMeta: String,
    @SerializedName("label")
    val label: String,
    @SerializedName("selectedChoiceLabel")
    val selectedChoiceLabel: String
)