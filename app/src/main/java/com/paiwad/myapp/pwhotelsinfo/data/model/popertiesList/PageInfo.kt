package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class PageInfo(
    @SerializedName("pageType")
    val pageType: String
)