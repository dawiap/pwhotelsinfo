package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class Pagination(
    @SerializedName("currentPage")
    val currentPage: Int,
    @SerializedName("nextPageGroup")
    val nextPageGroup: String,
    @SerializedName("nextPageNumber")
    val nextPageNumber: Int,
    @SerializedName("nextPageStartIndex")
    val nextPageStartIndex: Int,
    @SerializedName("pageGroup")
    val pageGroup: String
)