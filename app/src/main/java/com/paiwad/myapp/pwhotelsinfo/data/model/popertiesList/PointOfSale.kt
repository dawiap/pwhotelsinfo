package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class PointOfSale(
    @SerializedName("currency")
    val currency: Currency
)