package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class PointOfSaleX(
    @SerializedName("brandName")
    val brandName: String,
    @SerializedName("numberSeparators")
    val numberSeparators: String
)