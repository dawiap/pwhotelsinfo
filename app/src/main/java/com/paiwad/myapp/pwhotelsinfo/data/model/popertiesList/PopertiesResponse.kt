package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class PopertiesResponse(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("result")
    val result: String
)