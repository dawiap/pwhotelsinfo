package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class Price(
    @SerializedName("label")
    val label: String,
    @SerializedName("multiplier")
    val multiplier: Int,
    @SerializedName("range")
    val range: RangeX
)