package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class PriceX(
    @SerializedName("current")
    val current: String,
    @SerializedName("exactCurrent")
    val exactCurrent: Double,
    @SerializedName("old")
    val old: String
)