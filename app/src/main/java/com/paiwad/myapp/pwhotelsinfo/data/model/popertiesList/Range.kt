package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class Range(
    @SerializedName("max")
    val max: Max,
    @SerializedName("min")
    val min: Min
)