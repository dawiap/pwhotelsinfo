package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class RangeX(
    @SerializedName("increments")
    val increments: Int,
    @SerializedName("max")
    val max: MaxX,
    @SerializedName("min")
    val min: MinX
)