package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class RatePlan(
    @SerializedName("features")
    val features: Features,
    @SerializedName("price")
    val price: PriceX
)