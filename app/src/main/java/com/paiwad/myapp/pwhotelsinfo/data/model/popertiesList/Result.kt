package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class Result(
    @SerializedName("address")
    val address: Address,
    @SerializedName("badging")
    val badging: Badging,
    @SerializedName("coordinate")
    val coordinate: Coordinate,
    @SerializedName("deals")
    val deals: Deals,
    @SerializedName("geoBullets")
    val geoBullets: List<Any>,
    @SerializedName("guestReviews")
    val guestReviews: GuestReviews,
    @SerializedName("id")
    val id: Int,
    @SerializedName("isAlternative")
    val isAlternative: Boolean,
    @SerializedName("landmarks")
    val landmarks: List<Landmark>,
    @SerializedName("messaging")
    val messaging: Messaging,
    @SerializedName("name")
    val name: String,
    @SerializedName("neighbourhood")
    val neighbourhood: String,
    @SerializedName("optimizedThumbUrls")
    val optimizedThumbUrls: OptimizedThumbUrls,
    @SerializedName("pimmsAttributes")
    val pimmsAttributes: String,
    @SerializedName("providerType")
    val providerType: String,
    @SerializedName("ratePlan")
    val ratePlan: RatePlan,
    @SerializedName("starRating")
    val starRating: Double,
    @SerializedName("supplierHotelId")
    val supplierHotelId: Int,
    @SerializedName("urls")
    val urls: Urls
)