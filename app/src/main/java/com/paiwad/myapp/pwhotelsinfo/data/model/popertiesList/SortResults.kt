package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class SortResults(
    @SerializedName("distanceOptionLandmarkId")
    val distanceOptionLandmarkId: Int,
    @SerializedName("options")
    val options: List<Option>
)