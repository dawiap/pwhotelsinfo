package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class StarRating(
    @SerializedName("applied")
    val applied: Boolean,
    @SerializedName("items")
    val items: List<ItemXXXXX>
)