package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class Tracking(
    @SerializedName("omniture")
    val omniture: Omniture
)