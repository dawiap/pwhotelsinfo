package com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList


import com.google.gson.annotations.SerializedName

data class WelcomeRewards(
    @SerializedName("items")
    val items: List<ItemXXXXXXX>,
    @SerializedName("label")
    val label: String
)