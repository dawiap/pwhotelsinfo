package com.paiwad.myapp.pwhotelsinfo.data.model.reviews


import com.google.gson.annotations.SerializedName

data class GuestReview(
    @SerializedName("id")
    val id: String,
    @SerializedName("reviews")
    val reviews: List<Review>,
    @SerializedName("title")
    val title: String
)