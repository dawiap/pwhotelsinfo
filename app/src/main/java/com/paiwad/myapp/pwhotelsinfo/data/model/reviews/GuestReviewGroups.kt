package com.paiwad.myapp.pwhotelsinfo.data.model.reviews


import com.google.gson.annotations.SerializedName

data class GuestReviewGroups(
    @SerializedName("guestReviewOverview")
    val guestReviewOverview: GuestReviewOverview,
    @SerializedName("guestReviewPagination")
    val guestReviewPagination: GuestReviewPagination,
    @SerializedName("guestReviews")
    val guestReviews: List<GuestReview>
)