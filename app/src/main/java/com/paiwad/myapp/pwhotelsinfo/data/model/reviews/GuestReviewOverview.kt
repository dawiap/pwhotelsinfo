package com.paiwad.myapp.pwhotelsinfo.data.model.reviews


import com.google.gson.annotations.SerializedName

data class GuestReviewOverview(
    @SerializedName("cleanliness")
    val cleanliness: Double,
    @SerializedName("formattedCleanliness")
    val formattedCleanliness: String,
    @SerializedName("formattedHotelCondition")
    val formattedHotelCondition: String,
    @SerializedName("formattedHotelService")
    val formattedHotelService: String,
    @SerializedName("formattedOverall")
    val formattedOverall: String,
    @SerializedName("formattedRoomComfort")
    val formattedRoomComfort: String,
    @SerializedName("hotelCondition")
    val hotelCondition: Double,
    @SerializedName("hotelService")
    val hotelService: Int,
    @SerializedName("neighbourhood")
    val neighbourhood: Int,
    @SerializedName("overall")
    val overall: Double,
    @SerializedName("overallScoreBreakdown")
    val overallScoreBreakdown: List<OverallScoreBreakdown>,
    @SerializedName("qualitativeBadgeText")
    val qualitativeBadgeText: String,
    @SerializedName("roomComfort")
    val roomComfort: Int,
    @SerializedName("totalCount")
    val totalCount: Int
)