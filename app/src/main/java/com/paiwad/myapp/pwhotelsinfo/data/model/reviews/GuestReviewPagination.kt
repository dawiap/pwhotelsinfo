package com.paiwad.myapp.pwhotelsinfo.data.model.reviews


import com.google.gson.annotations.SerializedName

data class GuestReviewPagination(
    @SerializedName("nextPage")
    val nextPage: Boolean,
    @SerializedName("page")
    val page: Int,
    @SerializedName("previousPage")
    val previousPage: Boolean
)