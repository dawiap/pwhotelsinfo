package com.paiwad.myapp.pwhotelsinfo.data.model.reviews


import com.google.gson.annotations.SerializedName

data class Omniture(
    @SerializedName("evar34")
    val evar34: String
)