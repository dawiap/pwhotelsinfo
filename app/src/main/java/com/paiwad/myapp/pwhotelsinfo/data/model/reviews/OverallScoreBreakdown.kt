package com.paiwad.myapp.pwhotelsinfo.data.model.reviews


import com.google.gson.annotations.SerializedName

data class OverallScoreBreakdown(
    @SerializedName("amount")
    val amount: Int,
    @SerializedName("formattedScore")
    val formattedScore: String,
    @SerializedName("score")
    val score: Int
)