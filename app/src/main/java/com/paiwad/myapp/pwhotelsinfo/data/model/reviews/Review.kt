package com.paiwad.myapp.pwhotelsinfo.data.model.reviews


import com.google.gson.annotations.SerializedName

data class Review(
    @SerializedName("formattedRating")
    val formattedRating: String,
    @SerializedName("postedOn")
    val postedOn: String,
    @SerializedName("qualitativeBadgeText")
    val qualitativeBadgeText: String,
    @SerializedName("rating")
    val rating: Float,
    @SerializedName("recommendedBy")
    val recommendedBy: String,
    @SerializedName("reviewType")
    val reviewType: String,
    @SerializedName("summary")
    val summary: String,
    @SerializedName("title")
    val title: String
)