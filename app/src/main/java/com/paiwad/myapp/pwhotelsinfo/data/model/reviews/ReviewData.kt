package com.paiwad.myapp.pwhotelsinfo.data.model.reviews


import com.google.gson.annotations.SerializedName

data class ReviewData(
    @SerializedName("guestReviewGroups")
    val guestReviewGroups: GuestReviewGroups,
    @SerializedName("hotelId")
    val hotelId: String,
    @SerializedName("omniture")
    val omniture: Omniture
)