package com.paiwad.myapp.pwhotelsinfo.data.model.reviews


import com.google.gson.annotations.SerializedName

data class ReviewsResponse(
    @SerializedName("result")
    val result: String,
    @SerializedName("reviewData")
    val reviewData: ReviewData
)