package com.paiwad.myapp.pwhotelsinfo.data.remote

import com.paiwad.myapp.pwhotelsinfo.BuildConfig
import com.paiwad.myapp.pwhotelsinfo.data.model.locationsSearch.HotelsSearchResponse
import com.paiwad.myapp.pwhotelsinfo.data.model.photos.PhotosResponse
import com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail.PopertiesDetailResponse
import com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList.PopertiesResponse
import com.paiwad.myapp.pwhotelsinfo.data.model.reviews.ReviewsResponse
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Query

interface HotelsRapidApi {
    @Headers(
        value = [
            "x-rapidapi-key: ${BuildConfig.RAPIDAPI_KEY}",
            "x-rapidapi-host: ${BuildConfig.RAPIDAPI_HOST}",
            "useQueryString: ${true}"
        ]
    )
    @GET("/locations/search")
    fun locationsSearch(
        @Query("query") query: String,
        @Query("locale") locale: String = "th_TH",
    ): Observable<HotelsSearchResponse>

    @Headers(
        value = [
            "x-rapidapi-key: ${BuildConfig.RAPIDAPI_KEY}",
            "x-rapidapi-host: ${BuildConfig.RAPIDAPI_HOST}",
            "useQueryString: ${true}"
        ]
    )
    @GET("/properties/list")
    fun getProperties(
        @Query("adults1") adults1: Int = 1,
        @Query("pageNumber") pageNumber: Int = 1,
        @Query("destinationId") destinationId: String,
        @Query("pageSize") pageSize: Int = 25,
        @Query("checkOut") checkOut: String,
        @Query("checkIn") checkIn: String,
        @Query("sortOrder") sortOrder: String = "PRICE",
        @Query("locale") locale: String = "th_TH",
        @Query("currency") currency: String = "THB",
    ): Observable<PopertiesResponse>

    @Headers(
            value = [
                "x-rapidapi-key: ${BuildConfig.RAPIDAPI_KEY}",
                "x-rapidapi-host: ${BuildConfig.RAPIDAPI_HOST}",
                "useQueryString: ${true}"
            ]
    )
    @GET("/properties/get-details")
    fun getPropertiesDetail(
        @Query("id") id: String,
        @Query("checkOut") checkOut: String,
        @Query("checkIn") checkIn: String,
        @Query("currency") currency: String = "THB",
        @Query("locale") locale: String = "th_TH",
        @Query("adults1") adults1: Int = 1,
    ): Observable<PopertiesDetailResponse>

    @Headers(
            value = [
                "x-rapidapi-key: ${BuildConfig.RAPIDAPI_KEY}",
                "x-rapidapi-host: ${BuildConfig.RAPIDAPI_HOST}",
                "useQueryString: ${true}"
            ]
    )
    @GET("/reviews/list")
    fun getHotelReviews(
            @Query("id") id: String,
            @Query("page") page: Int = 1,
            @Query("loc") loc: String = "en_US"
    ): Observable<ReviewsResponse>

    @Headers(
            value = [
                "x-rapidapi-key: ${BuildConfig.RAPIDAPI_KEY}",
                "x-rapidapi-host: ${BuildConfig.RAPIDAPI_HOST}",
                "useQueryString: ${true}"
            ]
    )
    @GET("/properties/get-hotel-photos")
    fun getHotelPhotos(
            @Query("id") id: String,
    ): Observable<PhotosResponse>
}