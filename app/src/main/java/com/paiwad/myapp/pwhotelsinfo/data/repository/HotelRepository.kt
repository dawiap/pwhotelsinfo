package com.paiwad.myapp.pwhotelsinfo.data.repository

import com.paiwad.myapp.pwhotelsinfo.data.model.locationsSearch.HotelsSearchResponse
import com.paiwad.myapp.pwhotelsinfo.data.model.photos.PhotosResponse
import com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail.PopertiesDetailResponse
import com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList.PopertiesResponse
import com.paiwad.myapp.pwhotelsinfo.data.model.reviews.ReviewsResponse
import io.reactivex.rxjava3.core.Observable

interface HotelRepository {
    fun searchWithLocations(query: String): Observable<HotelsSearchResponse>

    fun getProperties(
            destinationId: String,
            checkOut: String,
            checkIn: String
    ): Observable<PopertiesResponse>

    fun getPropertiesDetail(
            id: String,
            checkOut: String,
            checkIn: String
    ): Observable<PopertiesDetailResponse>

    fun getHotelReviews(
            id: String
    ): Observable<ReviewsResponse>

    fun getHotelPhotos(
            id: String
    ): Observable<PhotosResponse>
}