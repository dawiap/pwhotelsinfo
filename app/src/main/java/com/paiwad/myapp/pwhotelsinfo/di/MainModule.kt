package com.paiwad.myapp.pwhotelsinfo.di

import android.app.Application
import android.content.Context
import com.paiwad.myapp.pwhotelsinfo.BuildConfig
import com.paiwad.myapp.pwhotelsinfo.data.HotelRepositoryImpl
import com.paiwad.myapp.pwhotelsinfo.data.remote.HotelsRapidApi
import com.paiwad.myapp.pwhotelsinfo.data.repository.HotelRepository
import com.paiwad.myapp.pwhotelsinfo.ui.adapter.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
class MainModule {

    @Provides
    fun provideRetrofit(): HotelsRapidApi{
        val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BuildConfig.BASE_URL)
            .build()
        return retrofit.create(HotelsRapidApi::class.java)
    }

    @Provides
    fun provideHotelRepository(remoteSource: HotelsRapidApi): HotelRepository{
        return HotelRepositoryImpl(remoteSource)
    }

    @Provides
    fun provideHotelEntityListAdapter() = ChildHotelListAdapter()

    @Provides
    fun provideHotelSuggestionGroupAdapter(listAdapterChild: ChildHotelListAdapter) =
            ParentHotelListAdapter(listAdapterChild)

    @Provides
    fun providePropertiesListAdapter() = PropertiesListAdapter()

    @Provides
    fun provideSliderImageAdapter() = SliderImageAdapter()

    @Provides
    fun provideChildOverviewListAdapter() = ChildOverviewListAdapter()

    @Provides
    fun provideParentOverviewAdapterAdapter(listAdapter: ChildOverviewListAdapter) =
        ParentOverviewListAdapter(listAdapter)

    @Provides
    fun providePagerAdapter(context: Application) = MyPagerAdapter(context)

    @Provides
    fun provideReviewListAdapter() = ReviewListAdapter()
}