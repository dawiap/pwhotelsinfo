package com.paiwad.myapp.pwhotelsinfo.ui

import android.app.ProgressDialog
import android.database.DatabaseUtils
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.paiwad.myapp.pwhotelsinfo.R
import com.paiwad.myapp.pwhotelsinfo.databinding.ActivityDetailBinding
import com.paiwad.myapp.pwhotelsinfo.ui.adapter.*
import com.paiwad.myapp.pwhotelsinfo.viewmodel.HotelDetailViewModel
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class DetailActivity : AppCompatActivity() {

    private val hotelDetailViewModel: HotelDetailViewModel by viewModels()
    @Inject
    lateinit var sliderImageAdapter: SliderImageAdapter
    @Inject
    lateinit var myPagerAdapter: MyPagerAdapter
    @Inject
    lateinit var parentOverviewListAdapter: ParentOverviewListAdapter
    @Inject
    lateinit var childOverviewListAdapter: ChildOverviewListAdapter
    @Inject
    lateinit var reviewListAdapter: ReviewListAdapter

    private lateinit var binding: ActivityDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        binding.lifecycleOwner = this

        val hotelId = intent.getIntExtra("hotel_id", 0)
        if (hotelId != 0) {
            hotelDetailViewModel.getObservableHotelDetail(hotelId.toString())
        }

        observeContent()
        observeState()
    }

    private fun observeState(){
        hotelDetailViewModel.message.observe(this,{
            it?.showMessage()
        })

        val progress = ProgressDialog(this@DetailActivity)
        hotelDetailViewModel.loading.observe(this, {
            if(it) {
                progress.show()
            }else {
                progress.dismiss()
            }
        })
    }

    private fun observeContent() {
        hotelDetailViewModel.hotelPhotosResponse.observe(this, {
            myPagerAdapter.items = it.hotelImages
            binding.pager.adapter = myPagerAdapter
            binding.tabDots.setupWithViewPager(binding.pager, true)
        })

        hotelDetailViewModel.hotelDetailResponse.observe(this, {
            binding.body = it.data.body
            parentOverviewListAdapter.items = it.data.body.overview.overviewSections
            binding.rcvOverview.adapter = parentOverviewListAdapter
        })

        hotelDetailViewModel.hotelReviewsResponse.observe(this, {
            if (it.reviewData.guestReviewGroups.guestReviews.isNotEmpty()) {
                it.reviewData.guestReviewGroups.guestReviews.first().apply {
                    reviewListAdapter.items = this.reviews
                    binding.review = this
                }
            }
            binding.rcvReview.adapter = reviewListAdapter
        })
    }

    private fun String.showMessage(){
        Toast.makeText(this@DetailActivity, this, Toast.LENGTH_LONG).show()
    }
}