package com.paiwad.myapp.pwhotelsinfo.ui

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.paiwad.myapp.pwhotelsinfo.R
import com.paiwad.myapp.pwhotelsinfo.databinding.ActivityMainBinding
import com.paiwad.myapp.pwhotelsinfo.ui.adapter.ChildHotelListAdapter
import com.paiwad.myapp.pwhotelsinfo.ui.adapter.ParentHotelListAdapter
import com.paiwad.myapp.pwhotelsinfo.viewmodel.HotelViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val hotelViewModel: HotelViewModel by viewModels()

    @Inject lateinit var parentHotelListAdapter: ParentHotelListAdapter

    @Inject lateinit var childHotelListAdapter: ChildHotelListAdapter

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.viewModel = hotelViewModel
        binding.lifecycleOwner = this

        hotelViewModel.searchHotelsWithLocation()

        binding.searchLocation.setOnEditorActionListener { _, actionId, _ ->
            if(actionId == EditorInfo.IME_ACTION_SEARCH){
                hotelViewModel.searchHotelsWithLocation()
                hideKeyboard()
            }
            true
        }

        observeState()
        setListAdapter()
    }

    private fun setListAdapter() {
        hotelViewModel.hotelsResponse.observe(this, {
            println(it.suggestions)
            parentHotelListAdapter.items = it.suggestions
            binding.rcv.adapter = parentHotelListAdapter

            parentHotelListAdapter.onItemClick = { view, entity ->
                hotelViewModel.goToPropertiesList(view, entity)
            }
        })
    }

    private fun observeState(){
        hotelViewModel.message.observe(this,{
            it?.showMessage()
        })

        val progress = ProgressDialog(this@MainActivity)
        hotelViewModel.loading.observe(this, {
            if(it) {
                progress.show()
            }else {
                progress.dismiss()
            }
        })
    }


    private fun String.showMessage(){
        Toast.makeText(this@MainActivity, this, Toast.LENGTH_LONG).show()
    }

    private fun Fragment.hideKeyboard() {
        view?.let { activity?.hideKeyboard(it) }
    }

    private fun Activity.hideKeyboard() {
        hideKeyboard(currentFocus ?: View(this))
    }

    @SuppressLint("ServiceCast")
    private fun Context.hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }
}