package com.paiwad.myapp.pwhotelsinfo.ui

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.paiwad.myapp.pwhotelsinfo.R
import com.paiwad.myapp.pwhotelsinfo.data.model.locationsSearch.Entity
import com.paiwad.myapp.pwhotelsinfo.databinding.ActivityPropertiesListBinding
import com.paiwad.myapp.pwhotelsinfo.ui.adapter.PropertiesListAdapter
import com.paiwad.myapp.pwhotelsinfo.viewmodel.HotelViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PropertiesListActivity : AppCompatActivity() {

    private val hotelViewModel: HotelViewModel by viewModels()

    @Inject lateinit var propertiesListAdapter: PropertiesListAdapter

    private lateinit var binding: ActivityPropertiesListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_properties_list)
        binding.viewModel = hotelViewModel
        binding.lifecycleOwner = this

        val entity = intent.getSerializableExtra("entity") as Entity
        hotelViewModel.getPropertiesList(entity)
        hotelViewModel.propertiesResponse.observe(this,{
            propertiesListAdapter.items = it.data.body.searchResults.results
            binding.rcvProperties.adapter = propertiesListAdapter
            propertiesListAdapter.onItemClick = {_, result ->
                val intent = Intent(this, DetailActivity::class.java)
                intent.putExtra("hotel_id",result.id)
                startActivity(intent)
            }
        })

        observeState()
    }

    private fun observeState(){
        hotelViewModel.message.observe(this,{
            it?.showMessage()
        })

        val progress = ProgressDialog(this@PropertiesListActivity)
        hotelViewModel.loading.observe(this, {
            if(it) {
                progress.show()
            }else {
                progress.dismiss()
            }
        })
    }

    private fun String.showMessage(){
        Toast.makeText(this@PropertiesListActivity, this, Toast.LENGTH_LONG).show()
    }
}