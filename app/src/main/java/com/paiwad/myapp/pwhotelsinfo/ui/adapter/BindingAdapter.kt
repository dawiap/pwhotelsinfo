package com.paiwad.myapp.pwhotelsinfo.ui.adapter

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.paiwad.myapp.pwhotelsinfo.data.model.locationsSearch.Entity

@BindingAdapter("app:itemsEntity")
fun setItemsEntity(rcv: RecyclerView, items: List<Entity>?){
    items?.let {
        (rcv.adapter as ChildHotelListAdapter).items = items
        (rcv.adapter as ChildHotelListAdapter).notifyDataSetChanged()
    }
}

@BindingAdapter("app:imageUrl")
fun setIImageUrl(imageView: ImageView, url: String?){
    url?.let {
        Glide.with(imageView.context)
            .load(it)
            .into(imageView)
    }
}

@BindingAdapter("app:detailImageUrl")
fun setIDetailImageUrl(imageView: ImageView, url: String?){
    url?.let {
        val base = it.substringBefore("_{size}")+it.substringAfter("_{size}")
        println("base : $base")
        Glide.with(imageView.context)
            .load(base)
            .into(imageView)
    }
}