package com.paiwad.myapp.pwhotelsinfo.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.paiwad.myapp.pwhotelsinfo.data.model.locationsSearch.Entity
import com.paiwad.myapp.pwhotelsinfo.databinding.CustomChildHotelBinding

class ChildHotelListAdapter: RecyclerView.Adapter<ChildHotelListAdapter.EntityViewHolder>() {

    var items = emptyList<Entity>()

    lateinit var onItemClick: ((View, Entity) -> Unit)

    inner class EntityViewHolder(private val binding: CustomChildHotelBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(entity: Entity, onItemClick: ((View, Entity) -> Unit)){
            binding.entity = entity
            binding.root.setOnClickListener {
                onItemClick(binding.root, entity)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EntityViewHolder {
        return EntityViewHolder(CustomChildHotelBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: EntityViewHolder, position: Int) {
        holder.bind(items[position], onItemClick)
    }

    override fun getItemCount(): Int {
        return items.size
    }
}