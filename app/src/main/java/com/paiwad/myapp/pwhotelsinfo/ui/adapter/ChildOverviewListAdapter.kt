package com.paiwad.myapp.pwhotelsinfo.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.paiwad.myapp.pwhotelsinfo.data.model.locationsSearch.Entity
import com.paiwad.myapp.pwhotelsinfo.databinding.CustomChildHotelBinding
import com.paiwad.myapp.pwhotelsinfo.databinding.CustomChildOverviewBinding

class ChildOverviewListAdapter: RecyclerView.Adapter<ChildOverviewListAdapter.ContentViewHolder>() {

    var items = emptyList<String>()

    inner class ContentViewHolder(private val binding: CustomChildOverviewBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(text: String){
            binding.textView.text = text
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContentViewHolder {
        return ContentViewHolder(CustomChildOverviewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ContentViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }
}