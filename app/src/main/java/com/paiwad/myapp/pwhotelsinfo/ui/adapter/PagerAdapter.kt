package com.paiwad.myapp.pwhotelsinfo.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.paiwad.myapp.pwhotelsinfo.data.model.photos.HotelImage
import com.paiwad.myapp.pwhotelsinfo.databinding.SliderItemBinding

class MyPagerAdapter(val context: Context): PagerAdapter() {

    lateinit var layoutInflater: LayoutInflater

    var items = emptyList<HotelImage>()

    override fun getCount(): Int {
        return items.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater = LayoutInflater.from(context)
        val binding = SliderItemBinding.inflate(LayoutInflater.from(layoutInflater.context),container,false)
        binding.photo = items[position]
        container.addView(binding.root,0)
        return binding.root
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        //super.destroyItem(container, position, `object`)
        container.removeView(`object` as View)
    }
}