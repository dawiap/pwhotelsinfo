package com.paiwad.myapp.pwhotelsinfo.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.paiwad.myapp.pwhotelsinfo.data.model.locationsSearch.Entity
import com.paiwad.myapp.pwhotelsinfo.data.model.locationsSearch.Suggestion
import com.paiwad.myapp.pwhotelsinfo.databinding.CustomParentHotelBinding

class ParentHotelListAdapter(
        private val listAdapterChild: ChildHotelListAdapter
): RecyclerView.Adapter<ParentHotelListAdapter.SuggestionGroupViewHolder>() {

    var items = emptyList<Suggestion>()

    lateinit var onItemClick: ((View, Entity) -> Unit)

    inner class SuggestionGroupViewHolder(private val binding: CustomParentHotelBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(suggestion: Suggestion, adapter: ChildHotelListAdapter){
            binding.suggestion = suggestion
            adapter.items = suggestion.entities
            binding.rcvEntity.adapter = adapter
            adapter.onItemClick = {view, entity ->
                onItemClick(view, entity)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SuggestionGroupViewHolder {
       return SuggestionGroupViewHolder(CustomParentHotelBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: SuggestionGroupViewHolder, position: Int) {
        holder.bind(items[position], listAdapterChild)
    }

    override fun getItemCount(): Int {
        return items.size
    }
}