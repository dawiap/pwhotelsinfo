package com.paiwad.myapp.pwhotelsinfo.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.paiwad.myapp.pwhotelsinfo.data.model.locationsSearch.Entity
import com.paiwad.myapp.pwhotelsinfo.data.model.locationsSearch.Suggestion
import com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail.OverviewSection
import com.paiwad.myapp.pwhotelsinfo.databinding.CustomParentHotelBinding
import com.paiwad.myapp.pwhotelsinfo.databinding.CustomParentOverviewBinding

class ParentOverviewListAdapter(
        private val listAdapterChild: ChildOverviewListAdapter
): RecyclerView.Adapter<ParentOverviewListAdapter.OverviewViewHolder>() {

    var items = emptyList<OverviewSection>()

    inner class OverviewViewHolder(private val binding: CustomParentOverviewBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(overviewSection: OverviewSection, adapter: ChildOverviewListAdapter){
            binding.section = overviewSection
            if(overviewSection.contentType == CONTENT_TYPE) {
                adapter.items = overviewSection.content
                binding.rcvOverviewContent.adapter = adapter
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OverviewViewHolder {
       return OverviewViewHolder(CustomParentOverviewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: OverviewViewHolder, position: Int) {
        holder.bind(items[position], listAdapterChild)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    companion object {
        const val CONTENT_TYPE = "LIST"
    }
}