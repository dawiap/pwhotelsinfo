package com.paiwad.myapp.pwhotelsinfo.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.paiwad.myapp.pwhotelsinfo.data.model.locationsSearch.Entity
import com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList.Result
import com.paiwad.myapp.pwhotelsinfo.databinding.CustomChildHotelBinding
import com.paiwad.myapp.pwhotelsinfo.databinding.CustomPropertiesListBinding

class PropertiesListAdapter: RecyclerView.Adapter<PropertiesListAdapter.PropertiesViewHolder>() {

    var items = emptyList<Result>()

    lateinit var onItemClick: ((View, Result) -> Unit)

    inner class PropertiesViewHolder(private val binding: CustomPropertiesListBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(result: Result, onItemClick: ((View, Result) -> Unit)){
            binding.result = result
            binding.root.setOnClickListener {
                onItemClick(binding.root, result)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PropertiesViewHolder {
        return PropertiesViewHolder(CustomPropertiesListBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: PropertiesViewHolder, position: Int) {
        holder.bind(items[position], onItemClick)
    }

    override fun getItemCount(): Int {
        return items.size
    }
}