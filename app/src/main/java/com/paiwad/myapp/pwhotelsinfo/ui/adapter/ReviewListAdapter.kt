package com.paiwad.myapp.pwhotelsinfo.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.paiwad.myapp.pwhotelsinfo.data.model.locationsSearch.Entity
import com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList.Result
import com.paiwad.myapp.pwhotelsinfo.data.model.reviews.Review
import com.paiwad.myapp.pwhotelsinfo.databinding.CustomChildHotelBinding
import com.paiwad.myapp.pwhotelsinfo.databinding.CustomPropertiesListBinding
import com.paiwad.myapp.pwhotelsinfo.databinding.CustomReviewsListBinding

class ReviewListAdapter: RecyclerView.Adapter<ReviewListAdapter.ReviewViewHolder>() {

    var items = emptyList<Review>()

    inner class ReviewViewHolder(private val binding: CustomReviewsListBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(review: Review){
            binding.review = review
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewViewHolder {
        return ReviewViewHolder(CustomReviewsListBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ReviewViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }
}