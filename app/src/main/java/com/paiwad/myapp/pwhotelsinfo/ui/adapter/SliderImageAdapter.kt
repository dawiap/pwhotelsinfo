package com.paiwad.myapp.pwhotelsinfo.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.paiwad.myapp.pwhotelsinfo.data.model.photos.HotelImage
import com.paiwad.myapp.pwhotelsinfo.databinding.SliderItemBinding
import com.smarteist.autoimageslider.SliderViewAdapter

class SliderImageAdapter: SliderViewAdapter<SliderImageAdapter.ImageViewHolder>() {

    var items = emptyList<HotelImage>()

    inner class ImageViewHolder(private val binding: SliderItemBinding): SliderViewAdapter.ViewHolder(binding.root){
        fun bind(item: HotelImage){
            binding.photo = item
        }
    }

    override fun getCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?): ImageViewHolder {
        return ImageViewHolder(SliderItemBinding.inflate(LayoutInflater.from(parent?.context),parent,false))
    }

    override fun onBindViewHolder(viewHolder: ImageViewHolder?, position: Int) {
        viewHolder?.bind(item = items[position])
    }
}