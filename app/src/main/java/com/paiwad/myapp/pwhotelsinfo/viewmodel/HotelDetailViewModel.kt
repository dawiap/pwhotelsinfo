package com.paiwad.myapp.pwhotelsinfo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.paiwad.myapp.pwhotelsinfo.data.model.photos.PhotosResponse
import com.paiwad.myapp.pwhotelsinfo.data.model.popertiesDetail.PopertiesDetailResponse
import com.paiwad.myapp.pwhotelsinfo.data.model.reviews.Review
import com.paiwad.myapp.pwhotelsinfo.data.model.reviews.ReviewsResponse
import com.paiwad.myapp.pwhotelsinfo.data.repository.HotelRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.coroutines.flow.Flow
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription
import javax.inject.Inject

@HiltViewModel
class HotelDetailViewModel @Inject constructor(private val repository: HotelRepository): ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    private val _hotelDetailResponse = MutableLiveData<PopertiesDetailResponse>()
    val hotelDetailResponse: LiveData<PopertiesDetailResponse> = _hotelDetailResponse

    private val _hotelPhotosResponse = MutableLiveData<PhotosResponse>()
    val hotelPhotosResponse: LiveData<PhotosResponse> = _hotelPhotosResponse

    private val _hotelReviewsResponse = MutableLiveData<ReviewsResponse>()
    val hotelReviewsResponse: LiveData<ReviewsResponse> = _hotelReviewsResponse

    private val _message = MutableLiveData<String?>()
    val message: LiveData<String?> = _message

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean> = _loading

    private fun concatHotelCalls(id: String): Observable<Any> {
        return Observable.concat(
                repository.getPropertiesDetail(id, "2021-06-08", "2021-06-09"),
                repository.getHotelPhotos(id),
                repository.getHotelReviews(id))
    }

    private fun mergeHotelCalls(id: String): Observable<Any> {
        return Observable.merge(
                repository.getPropertiesDetail(id, "2021-06-08", "2021-06-09"),
                repository.getHotelPhotos(id),
                repository.getHotelReviews(id))
    }

    fun getObservableHotelDetail(hotelId: String) {
        _loading.postValue(true)
        val disposable = mergeHotelCalls(hotelId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnComplete {
                    _loading.postValue(false)
                }
                .subscribe({
                    when(it){
                        is PopertiesDetailResponse -> {
                            _hotelDetailResponse.postValue(it)
                        }
                        is PhotosResponse -> {
                            _hotelPhotosResponse.postValue(it)
                        }
                        is ReviewsResponse -> {
                            _hotelReviewsResponse.postValue(it)
                        }
                    }
                },{
                    _loading.postValue(false)
                    _message.postValue(it.message)
                })
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}