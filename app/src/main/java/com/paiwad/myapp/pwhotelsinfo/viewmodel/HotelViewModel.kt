package com.paiwad.myapp.pwhotelsinfo.viewmodel

import android.content.Intent
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.paiwad.myapp.pwhotelsinfo.data.model.locationsSearch.Entity
import com.paiwad.myapp.pwhotelsinfo.data.model.locationsSearch.HotelsSearchResponse
import com.paiwad.myapp.pwhotelsinfo.data.model.locationsSearch.Suggestion
import com.paiwad.myapp.pwhotelsinfo.data.model.popertiesList.PopertiesResponse
import com.paiwad.myapp.pwhotelsinfo.data.repository.HotelRepository
import com.paiwad.myapp.pwhotelsinfo.ui.PropertiesListActivity
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class HotelViewModel @Inject constructor(private val repository: HotelRepository): ViewModel() {

    val locationQuery = MutableLiveData<String?>("สกลนคร")

    private val compositeDisposable = CompositeDisposable()

    private val _hotelsResponse = MutableLiveData<HotelsSearchResponse>()
    val hotelsResponse: LiveData<HotelsSearchResponse> = _hotelsResponse

    private val _propertiesResponse = MutableLiveData<PopertiesResponse>()
    val propertiesResponse: LiveData<PopertiesResponse> = _propertiesResponse

    private val _message = MutableLiveData<String?>()
    val message: LiveData<String?> = _message

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean> = _loading

    fun searchHotelsWithLocation(){
        _loading.postValue(true)
        locationQuery.value?.let {query ->
            val disposable = repository.searchWithLocations(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete {
                    _loading.postValue(false)
                }
                .subscribe({
                    _hotelsResponse.postValue(it)
                    _message.postValue(null)
                },{
                    _loading.postValue(false)
                    _message.postValue(it.message)
                })
            compositeDisposable.add(disposable)

        } ?: _message.postValue("Please enter your query!")
    }

    fun getPropertiesList(entity: Entity){
        _loading.postValue(true)
        val disposable = repository.getProperties(destinationId = entity.destinationId,"2021-06-08","2021-06-09")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete {
                    _loading.postValue(false)
                }
                .subscribe({
                    _propertiesResponse.postValue(it)
                    _message.postValue(null)
                },{
                    _loading.postValue(false)
                    _message.postValue(it.message)
                })
        compositeDisposable.add(disposable)
    }

    fun goToPropertiesList(v: View, entity: Entity){
        val intent = Intent(v.context, PropertiesListActivity::class.java)
        intent.putExtra("entity", entity)
        v.context.startActivity(intent)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}